"use strict";

/*
  Вам необходимо добавить обработчики событий в соотвествии с заданием.
  Вам уже даны готовые стили и разметка. Менять их не нужно,
  за исключением навешивания обработчиков и добавления data - аттрибутов
*/

/**
 * Задание 1
 * Необходимо сделать выпадающее меню.
 * Меню должно открываться при клике на кнопку,
 * закрываться при клике на кнопку при клике по пункту меню
 */

document.getElementById("menuBtn").addEventListener("click", toggleMenu);
document.querySelector(".menu-list").addEventListener("click", toggleMenu);

function toggleMenu() {
  let menuContainer = document.getElementById("menu");
  menuContainer.classList.toggle("menu-opened");
}

/**
 * Задание 2
 * Необходимо реализовать перемещение блока по клику на поле.
 * Блок должен перемещаться в место клика.
 * При выполнении задания вам может помочь свойство element.getBoundingClientRect()
 */

document.getElementById("field").addEventListener("click", (event) => {
  const clickCoords = { x: event.clientX, y: event.clientY };

  const block = document.getElementById("movedBlock");
  const {
    x: fieldCoordsX,
    y: fieldCoordsY,
  } = event.currentTarget.getBoundingClientRect();

  let topPosition = clickCoords.y - fieldCoordsY;
  let leftPosition = clickCoords.x - fieldCoordsX;

  block.style.top = `${topPosition}px`;
  block.style.left = `${leftPosition}px`;
});

/**
 * Задание 3
 * Необходимо реализовать скрытие сообщения при клике на крестик при помощи делегирования
 */

document.getElementById("messager").addEventListener("click", (event) => {
  const { target } = event;

  if (target.classList.contains("remove")) {
    const messageToDelete = target.parentElement;
    messageToDelete.remove();
  }
});

/**
 * Задание 4
 * Необходимо реализовать вывод сообщения при клике на ссылку.
 * В сообщении спрашивать, что пользователь точно хочет перейти по указанной ссылке.
 * В зависимости от ответа редиректить или не редиректить пользователя
 */

document.getElementById("links").addEventListener("click", (event) => {
  const isSure = confirm("Вы действительно хотите прейти по ссылке?");

  if (!isSure) {
    event.preventDefault();
  }
});

/**
 * Задание 5
 * Необходимо сделать так, чтобы значение заголовка изменялось в соответствии с измением
 * значения в input
 */

document.getElementById("fieldHeader").addEventListener("input", (event) => {
  let header = document.getElementById("taskHeader");

  header.textContent = event.target.value;
});
